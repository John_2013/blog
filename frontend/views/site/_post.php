<?php
/**
 * Created by PhpStorm.
 * User: 2013
 * Date: 02.12.2015
 * Time: 13:11
 */
use yii\web\Link;
use yii\helpers\Url;
use yii\helpers\Html;

Yii::$app->formatter->locale = 'ru-RU';
?>
<div class="article" data-cat="<?= $model->category->id ?>">
    <?php echo Html::a(//сылка
        '<h2 class="post-preview">'.$model->title.'</h2>',//текст ссылки
        [
            'post',//адрес ссылки
            'id' => $model->id,//параметр(ы) ссылки
        ]
    );
    ?>

    <h3 class="post-subtitle" data-id="<?= $model->id ?>">
        <?= $model->anons ?>
    </h3>

    <p class="post-meta">
        Запостил <a href="#"><?= $model->author->nickname ?></a>
        от <?= Yii::$app->formatter->asDate($model->publish_date,'d.M.Y').' г.' ?>
    </p>

</div>
<hr>